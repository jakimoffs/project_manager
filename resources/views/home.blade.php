@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">
                        <div class="float-left">
                            Projects Dashboard
                        </div>
                        <div class="float-right dropdown">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                All Projects
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                @foreach($projects as $item)
                                    <a class="dropdown-item"
                                       href="{{route('project.show',$item)}}">{{$item->name}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @if($project)
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <h2>{{$project->name}}</h2>
                            @if($project->user_id === \Illuminate\Support\Facades\Auth::user()->id)
                                <form class="mb-4" action="{{route('card.create')}}" method="post">
                                    @csrf
                                    @method('post')
                                    <div class="form-group">
                                        <label for="project_name">Project name</label>
                                        <input class="form-control" value="{{old('name')}}" name="name" type="text"
                                               id="project_name">
                                        @if($errors->has('name'))
                                            <div class="text-danger">{{ $errors->first('name') }}</div>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="project_deadline">Deadline</label>
                                        <input type="date" value="{{old('deadline')}}" class="form-control"
                                               name="deadline" id="project_deadline">
                                        @if($errors->has('name'))
                                            <div class="text-danger">{{ $errors->first('deadline') }}</div>
                                        @endif
                                    </div>
                                    <input type="hidden" name="project_id" value="{{$project->id}}">
                                    <div class="form-group">
                                        <label for="project_status">Status</label>
                                        @if(count($statuses))
                                        <select class="form-control" name="status" value="{{old('status')}}"
                                                id="project_status">
                                            @forelse($statuses as $status)
                                                <option @if(old('status') === $status->name) selected
                                                        @endif value="{{$status->name}}">{{$status->name}}</option>
                                            @empty
                                                No statuses
                                            @endforelse
                                        </select>
                                        @else
                                            <input value="{{old('status')}}" type="text" name="status" id="project_status" class="form-control">
                                        @endif
                                        @if($errors->has('name'))
                                            <div class="text-danger">{{ $errors->first('status') }}</div>
                                        @endif
                                    </div>

                                    <button class="btn btn-success" type="submit">Create</button>
                                </form>
                                <draggable-list :project="{{$project}}"></draggable-list>
                            @else
                                <div class="list-group">

                                    @forelse($project->cards as $card)
                                        <span
                                            class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{$card->name}}</h5>
                                        <small>Order: {{$card->order}}</small>
                                    </div>
                                    <small>{{$card->status}}</small>
                                    </span>
                                    @empty
                                        Empty list
                                    @endforelse
                                    @endif
                                </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
@endsection
