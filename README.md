## Instruction

1) Copy .env.example to .env

2) Create database and write username, password, host, port into .env

3) Install composer `conposer install`

4) Install node_modules `npm install`

5) Run `npm run dev` 

6) Run `php artisan key:generate`

7) Run `php artisan migrate --seed`

8) Default User: `email: admin@admin.com password:secret`
