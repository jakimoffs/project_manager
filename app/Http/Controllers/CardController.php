<?php

namespace App\Http\Controllers;

use App\Card;
use App\Project;
use App\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CardController extends Controller
{
    public function update(Request $request, Card $card)
    {
        try {
            $request->validate([
                'name' => 'required|string',
                'deadline' => 'required|string',
                'status' => 'required|string',
            ]);
            $data = $request->all();
            $data['deadline'] = Carbon::parse($request->get('deadline'))->format('d-m-Y');
            $result = $card->update($data);
            if ($result) {
                return response('Updated!', 200);
            } else {
                return response(json_encode($result), 200);
            }
        } catch (\Exception $exception) {
            return response('Error!', 200);
        }
    }

    public function updateAll(Request $request)
    {
        try {
            $cards = $request->get('cards');
            foreach ($cards as $card) {
                $oldCard = Card::find($card['id']);
                $oldCard->order = $card['order'];
                $oldCard->save();
            }
            return response('Updated successfully', 200);

        } catch (\Exception $exception) {
            return response($exception->getMessage(), 500);
        }
    }

    public function store(Request $request)
    {
        $request->validate(['name' => 'required|string', 'status' => 'required']);
        try {
            $newStatus = $request->get('status');
            $status = Status::where('name', $request->get('status'))->first();
            if (!$status) {
                Status::create(['name'=>$newStatus]);
            }
            $data = $request->all();
            $data['deadline'] = Carbon::parse($request->get('deadline'))->format('d-m-Y');
            $data['order'] = 0;
            $project = Project::find($request->get('project_id'));
            $result = $project->cards()->create($data);
            if ($result) {
                return redirect()->back();
            }
        } catch (\Exception $exception) {
            return redirect()->back()->withInput();
        }

    }

    public function remove(Request $request, Card $card)
    {
        if (!$card) return response('No card found', 404);
        $card->delete();
        return response('Card deleted!', 200);
    }
}
