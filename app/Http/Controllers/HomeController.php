<?php

namespace App\Http\Controllers;

use App\Project;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $projects = Project::all();

        $project = Auth::user()->projects()->latest()->first();
        if ($project) {
            $project->load(['cards' => function ($q) {
                $q->orderBy('order', 'asc');
            }]);
        }
        $statuses = Status::all();
        return view('home', compact('projects', 'project', 'statuses'));
    }

    public function allStasuses()
    {
        return response()->json(Status::all());
    }
}
