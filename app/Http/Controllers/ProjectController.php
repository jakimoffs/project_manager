<?php

namespace App\Http\Controllers;


use App\Project;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    public function __construct()
    {
    }

    public function show(Request $request, Project $project)
    {
        $projects = Project::all();

        if ($project) {
            $project->load(['cards' => function ($q) {
                $q->orderBy('order', 'asc');
            }]);
        }
        $statuses = Status::all();

        return view('home', compact('projects', 'project', 'statuses'));
    }

    public function statusStore(Request $request)
    {
        $request->validate(['name' => 'required']);
        $status = Status::create($request->all());
        if ($status) {
            return redirect()->back();
        }
        return redirect()->back()->withInput();

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required|string']);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        try {
            Auth::user()->projects()->create($request->all());
            return redirect()->back();
        } catch (\Exception $exception) {
            Log::error("{$exception->getCode()} {$exception->getLine()} {$exception->getMessage()}");
            return redirect()->back()->withInput($request->all());
        }
    }
}
