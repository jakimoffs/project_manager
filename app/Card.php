<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Card extends Model
{
    protected $fillable = [
        'project_id',
        'name',
        'status',
        'deadline',
        'order',
    ];

    protected $appends = [
        'deadlineEnd'
    ];

    public function getDeadlineEndAttribute()
    {
        if ($this->deadline) {
            return Carbon::parse( $this->deadline)->diffForHumans();
        }
        return '';
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
