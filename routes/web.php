<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ProjectController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/project', 'HomeController@index')->name('home');
Route::get('/statuses/all', 'HomeController@allStasuses')->name('status.all');
Route::post('/project/store', 'ProjectController@store')->name('project.create');
Route::post('/status/store', 'ProjectController@statusStore')->name('status.store');
Route::get('/project/{project}', 'ProjectController@show')->name('project.show');
Route::put('/card/{card}/update', 'CardController@update')->name('card.update');
Route::put('/card/updateAll', 'CardController@updateAll')->name('card.updateAll');
Route::post('/card/updateAll', 'CardController@store')->name('card.create');
Route::delete('/card/delete/{card}', 'CardController@remove')->name('card.remove');
